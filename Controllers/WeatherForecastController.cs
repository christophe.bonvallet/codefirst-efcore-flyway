﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Formation.CodeFirst.Service.Context;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Formation.CodeFirst.Service.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;
        private readonly ISqlContext _context;

        public WeatherForecastController(ILogger<WeatherForecastController> logger, ISqlContext context)
        {
            _logger = logger;
            _context = context;
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            var entities = _context.TakeLastFive().ToArray();
            var models = entities.Select((e) => {
                return new WeatherForecast()
                {
                    Date = e.Date,
                    TemperatureC = e.TemperatureC,
                    Summary = e.Summary
                };
            });

            return models;
        }

        [HttpPost]
        public ActionResult Post([FromBody] WeatherForecast model)
        {
            var entity = new WeatherForecastEntity()
            {
                Date = model.Date,
                TemperatureC = model.TemperatureC,
                Summary = model.Summary
            };
            
            _context.WeatherForecast.Add(entity);
            _context.SaveChanges();

            return new OkResult();
        }
    }
}
