using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Formation.CodeFirst.Service.Context
{
    [Table("forecast")]
    public class WeatherForecastEntity
    {
        [Key()]
        [Column("id", TypeName="bigint")]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Column("date", TypeName="timestamp")]
        public DateTime Date { get; set; }

        [Column("tempc", TypeName="integer")]
        public int TemperatureC { get; set; }

        [Column("summary", TypeName="character varying")]
        public string Summary { get; set; }
    }
}
