using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Collections.Generic;

namespace Formation.CodeFirst.Service.Context
{
    public class SqlContext : DbContext, ISqlContext
    {
        public SqlContext(DbContextOptions options) : base(options) {}

        public DbSet<WeatherForecastEntity> WeatherForecast { get; set; }

        public IEnumerable<WeatherForecastEntity> TakeLastFive()
        {
            var query = this.WeatherForecast;

            return query.ToList().TakeLast(5);
        }

        public override int SaveChanges()
        {
            return base.SaveChanges();
        }
    }
}