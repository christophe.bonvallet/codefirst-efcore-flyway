using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace Formation.CodeFirst.Service.Context
{
    public interface ISqlContext
    {
        DbSet<WeatherForecastEntity> WeatherForecast { get; set; }
        
        int SaveChanges();

        IEnumerable<WeatherForecastEntity> TakeLastFive();
    }
}